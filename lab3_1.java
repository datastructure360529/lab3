import java.util.Scanner;

public class lab3_1 {
        public static int mySqrt(int x) {
        if (x <= 1) {
            return x; 
        }
        
        int left = 1;
        int right = x;
        int result = 0;
        
        while (left <= right) {
            int mid = left + (right - left) / 2;
            
            if (mid <= x / mid) {
                left = mid + 1;
                result = mid;
            } else {
                right = mid - 1;
            }
        }
        
        return result;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x = kb.nextInt();
        int result = mySqrt(x);

        System.out.println("Square root of " + x + "=" + result);
    }

        
}
